# 零基础编程教学

教程将会使用 `Vue`（网页框架）和 `TypeScript`（编程语言）作为入门教学。将会教导学者完成网站页面的开发。

对学者的要求：
- 电脑：Windows10 以上，8G 内存好一些
- 年龄：10岁 ~ 127岁
- 英语：无所谓，代码以拼音为主，多记几个单词
- 数学：吊毛，你最好懂点乘除和集合逻辑，数学在后期会是技术员的上限

## 为什么选择 GitLab
花了15分钟调研，平台需满足几个要素：
- 首要网络通畅，免费
- 使用方便，中文界面，注册简单，不要手机号
- 安全，不要莫名的被封掉，或者被平台修改内容

## 开启账号

点击打开 [gitlab.com/users/sign_up](gitlab.com/users/sign_up) 网站注册账号。过程可能需要人机验证，等待十来秒点击验证。

- 填写都用英文吧，不行就拼音
- 用户名，不可重复，可尝试加数字和 `-`,`_`,`.` 这几种符号
- 邮箱，没有接触过邮箱的就用 qq 邮箱吧，如 `111222333@qq.com`格式`@`前面就是qq号，后面登录邮箱获取验证码。
- ![sign_up](doc/img/00_sign_up.png)
- 下一步，邮箱验证之后，界面就选择这些吧 
- ![welcome](doc/img/00_welcome.png)
- 下一步，要求新建一个组，这里填写注册时的`用户名`，因为隐形要求唯一（URL），所以填`用户名`。下一步就进到个人面板了。

## Windows 电脑环境安装

### winget
Windows10 默认安装有了，如果没有可以在应用商店搜索安装。

网页搜索信息的时候推荐使用 Bing 搜索。

### 安装开发软件

``` bash
# 安装 Git，官网下载：https://git-scm.com/downloads，点击 windows 版本
winget install Git 

# 安装 Node.js，官网下载：https://nodejs.org/
winget install Node.js 

# 安装 Visual Studio Code，官网下载：https://code.visualstudio.com/
winget install Microsoft.VisualStudioCode 

```

### 安装开发环境

``` bash
# 安装 pnpm (快速、可靠的包管理器)
npm install -g pnpm

# 安装 Vue CLI (Vue.js 脚手架工具)
npm install -g @vue/cli

# 安装 TypeScript
npm install -g typescript

```

## 设置 SSH


## Clone 项目，启动！




## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/jiao357/CodeJiaoXue.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/jiao357/CodeJiaoXue/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
